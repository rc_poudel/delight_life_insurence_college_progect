
//import java.sql.DriverManager;
//import java.sql.SQLException;
//import java.util.Properties;


package dlic;
import java.sql.*;
/**
 *
 * @author rc
 */
public class MysqlConnection {
    
    // init database constants
//    private static final String DATABASE_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DATABASE_URL = "jdbc:mysql://localhost/dlic_db";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "yagiten";

    
    // connect database
    public static void main(String[] args){
        Connection conn = null;
        
        try{
            conn = DriverManager.getConnection(DATABASE_URL,USERNAME,PASSWORD);
            System.out.println("Connected !");
        }catch(Exception e){
            System.err.println(e);
        }
    }
   
}
